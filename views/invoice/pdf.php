<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\Items;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */

$this->title = $model->invoice_id;
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="invoice-view">

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 type">
                <div class="form-group">
                    <label>Invoive No: <?= $model->invoice_no; ?></label>               
                </div>
            </div>
            <div class="col-md-6 type">
                <div class="form-group">
                    <label>Date: <?= date('d-m-Y',strtotime($model->invoice_date)); ?></label>
                </div>
            </div>
        </div>
    </div>

    <table class="table table-bordered">  
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>Amount</th>
                </tr> 
            </thead> 
            <tbody class="invoice-items">
                <?php

                    $items = Items::find()->Where(['invoice_id'=>$model->invoice_id])->all();
                    $withouttax = 0;
                    $withtax = 0;
                    foreach($items as $key => $item):

                        $amount = $item->quantity*$item->price;
                        $withouttax+= $amount;
                        $tax = ($item->quantity*$item->price*$item->tax)/100;
                        $totalamt = $amount + $tax;
                        $totalamt_text = number_format($totalamt,2);
                        $withtax+= $totalamt;

                ?>
                <tr id="row0">
                    <td><?= ($key+1) ?></td>
                    <td><?= $item->name ?></td>
                    <td><?= $item->quantity ?></td>
                    <td><?= '$'.$item->price ?></td>
                    <td><?= $item->tax.'%' ?></td>
                    <td style="text-align:right;"><?= '$'.$totalamt_text; ?></td>
                </tr>
                <?php 
                    endforeach; 

                    if($model->discount_type==0){
                        $discount_type = '$'.number_format($model->discount,2);
                        $dis_amt = $model->discount;
                    }
                    else{
                        $discount_type = $model->discount.'%';
                        $dis_amt = ($withtax*$model->discount)/100;
                    }

                    $totalamount = $withtax - $dis_amt;

                    $withouttax_text = number_format($withouttax,2);

                    $withtax_text = number_format($withtax,2);

                    $totalamount_text = number_format($totalamount,2);

                ?>
                <tr id="sub-total">
                    <td colspan="5"><strong>Sub Total (with out tax)</strong></td>    
                    <td style="text-align:right;"><?= '$'.$withouttax_text; ?></td>   
                </tr>  
                <tr id="discount">
                    <td colspan="5">
                        <strong>Discount</strong>
                    </td> 
                    <td style="text-align:right;"><?= $discount_type ?></td>          
                </tr>
                <tr id="sub-total-tax">
                    <td colspan="5">
                        <strong>Sub Total (with tax)</strong>
                    </td> 
                    <td style="text-align:right;"><?= '$'.$withtax_text; ?></td>          
                </tr> 
                <tr id="total-amount">
                    <td colspan="5"><strong>Total Amount</strong></td> 
                    <td style="text-align:right;"><?= '$'.$totalamount_text; ?></td>          
                </tr> 
            </tbody>
        </table> 

</div>
