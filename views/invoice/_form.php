<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Invoice */
/* @var $form yii\widgets\ActiveForm */
?>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="<?php echo Yii::$app->request->baseUrl; ?>/jsnew/invoice.js" type="text/javascript"></script>

<div class="invoice-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-3 type">
                <div class="form-group">
                    <label>Invoive No.</label>
                    <input type="text" class="form-control invoiceno" name="invoice_no" id="invoiceno" placeholder="Invoive No">
                    <span class="error"></span>
                </div>
            </div>
            <div class="col-md-3 type">
                <div class="form-group">
                    <label>Date</label>
                    <input type="date" class="form-control datepicker" name="invoice_date" id="datepicker" value="<?php echo date("Y-m-d") ?>"> 
                    <span class="error"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <table class="table table-bordered">  
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Tax</th>
                    <th>Amount</th>
                    <th></th>
                </tr> 
            </thead> 
            <tbody class="invoice-items">
                <tr id="row0">
                    <td>1</td>
                    <td>
                        <input type="text" class="form-control name" id="name_0" placeholder="Name" name="name[]" data-id="0">
                        <span class="error"></span>
                    </td>
                    <td>
                        <input type="number" class="form-control quantity itemvalue" id="quantity_0" placeholder="Quantity" name="quantity[]" data-id="0" min="0" oninput="this.value = Math.abs(this.value)">
                        <span class="error"></span>
                    </td>
                    <td>
                        <div class="input-group mb-2" id="input-group_0">
                            <div class="input-group-prepend">
                              <div class="input-group-text">$</div>
                            </div>
                            <input type="number" class="form-control price itemvalue" id="price_0" placeholder="Price" name="price[]" data-id="0" min="0" oninput="this.value = Math.abs(this.value)">
                        </div>
                        <span class="error"></span>
                    </td>
                    <td>
                        <select class="form-control tax itemvalue" id="tax_0" name="tax[]" data-id="0">
                            <option value="0" selected>0%</option>
                            <option value="1">1%</option>
                            <option value="5">5%</option>
                            <option value="10">10%</option>
                        </select>
                    </td>
                    <td id="total_0"></td>
                    <td class="icon-groups">
                        <input type="hidden" class="sub-without-tax" id="sub-without-tax_0" value="">
                        <input type="hidden" class="sub-with-tax" id="sub-with-tax_0" value="">
                        <a href="javascript:void(0);" class="btn btn-primary addmore" id="addmore"><i class="fa fa-plus"></i></a>
                    </td>
                </tr>
                <tr id="sub-total">
                    <td colspan="5"><strong>Sub Total (with out tax)</strong></td> 
                    <td><strong id="total-without-tax"></strong></td>          
                </tr>  
                <tr id="discount">
                    <td colspan="5">
                        <strong>Discount</strong>
                    </td> 
                    <td>
                        <div class="disauto">
                            <input type="number" class="form-control discount-value" id="discount-value" placeholder="Discount" name="discount" value="" style="width:60%;">
                            <input type="radio" id="dollar" name="discount-radio" value="0" class="radio-butn" checked="checked">
                            <label for="dollar">$</label>
                            <input type="radio" id="percent" name="discount-radio" value="1" class="radio-butn">
                            <label for="percent">%</label>
                        </div>
                    </td>          
                </tr>
                <tr id="sub-total-tax">
                    <td colspan="5">
                        <strong>Sub Total (with tax)</strong>
                    </td> 
                    <td>
                        <strong id="total-with-tax"></strong>
                        <input type="hidden" id="sub-with-tax-total" value="">
                    </td>          
                </tr> 
                <tr id="total-amount">
                    <td colspan="5"><strong>Total Amount</strong></td> 
                    <td>
                        <strong id="total-amt-text"></strong>
                        <input type="hidden" id="total-amt" name="total-amt" value="">
                    </td>          
                </tr> 
            </tbody>
        </table> 
    </div>

    <?php ActiveForm::end(); ?>

    <div class="col-md-12 text-center">
        <div class="form-groups">
            <button class="btn btn-primary save-invoice" id="save-invoice"> Save </button>
            <button class="btn btn-danger cancel cancel-invoice" id="cancel-invoice" onclick="location.href='<?php echo Yii::$app->request->baseUrl; ?>/invoice/index'"> Cancel </button>
        </div>
    </div>

</div>
