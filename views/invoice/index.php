<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoice-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Invoice', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'invoice_id',
            'invoice_no',
            //'invoice_date',
            [ 
              'label' => 'Invoice Date',
              'headerOptions' => ['style' => 'color:#007bff'],
              'value' => function ($model) { 
                    $date = date("d-m-Y",  strtotime($model->invoice_date)); 
                    return  $date;
                },
            ],
            //'discount',
            [ 
              'label' => 'Discount',
              'headerOptions' => ['style' => 'color:#007bff'],
              'value' => function ($model) { 
                    if($model->discount_type==0){
                        $discount = '$'.$model->discount;
                    } 
                    else{
                        $discount = $model->discount.'%';
                    }
                    return  $discount;
                },
            ],
            //'amount',
            [ 
              'label' => 'Amount',
              'headerOptions' => ['style' => 'color:#007bff'],
              'value' => function ($model) { 
                    return  '$'.$model->amount;
                },
            ],
            //'status',
            //'created_by',
            //'created_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [

                    'view' => function ($url,$model,$key) {

                        return Html::a('Generate Invoice', $url);

                    },

                ],

                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='view?invoice_id='.$model->invoice_id;
                        return $url;
                    }
          
                }

            ],
        ],
    ]); ?>


</div>
