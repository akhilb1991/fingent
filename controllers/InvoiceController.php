<?php

namespace app\controllers;

use Yii;
use kartik\mpdf\Pdf;
use app\models\Invoice;
use app\models\Items;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use amnah\yii2\user\models\User;
use yii\filters\AccessControl;

/**
 * InvoiceController implements the CRUD actions for Invoice model.
 */
class InvoiceController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['login'],
                            'roles' => ['?'],
                        ],
                        [
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Invoice::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'invoice_id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoice model.
     * @param int $invoice_id Invoice ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($invoice_id)
    {
        $content = $this->renderPartial('pdf',[
            'model' => $this->findModel($invoice_id),
        ]);
    
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        return $pdf->render();
        
        /*return $this->render('view', [
            'model' => $this->findModel($invoice_id),
        ]);*/
    }

    /**
     * Creates a new Invoice model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Invoice();

        if ($this->request->isPost) {
            $uid = Yii::$app->user->Id; 
            //echo 'hai'; exit;
            $model->invoice_no = $_POST['invoice_no'];
            $model->invoice_date = $_POST['invoice_date'];
            $model->discount = $_POST['discount'];
            $model->discount_type = $_POST['discount-radio'];
            $model->amount = $_POST['total-amt'];
            $model->status = 0;
            $model->created_by = $uid;
            if($model->save(false)):

                $count = count($_POST['name']);

                for ($i = 0; $i < $count; $i++) {

                    $item = new Items();
                    $item->invoice_id = $model->invoice_id;
                    $item->name = $_POST['name'][$i];
                    $item->quantity = $_POST['quantity'][$i];
                    $item->price = $_POST['price'][$i];
                    $item->tax = $_POST['tax'][$i];
                    $item->created_by = $uid;
                    $item->status = 0;
                    $item->save(false);

                }

            endif;
            //if ($model->load($this->request->post()) && $model->save()) {
            return $this->redirect('index');
            //}
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Invoice model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $invoice_id Invoice ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($invoice_id)
    {
        $model = $this->findModel($invoice_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'invoice_id' => $model->invoice_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Invoice model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $invoice_id Invoice ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($invoice_id)
    {
        $this->findModel($invoice_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $invoice_id Invoice ID
     * @return Invoice the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($invoice_id)
    {
        if (($model = Invoice::findOne($invoice_id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
