$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".invoice-items"); //Fields wrapper
    var add_button      = $(".addmore"); //Add button ID

    var x = 1; //initlal text box count
    var y = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click

        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            //text box increment
            $('#sub-total').before('<tr id="row'+x+'">' +
                '<td>'+(x+1)+'</td>' +
                '<td><input type="text" class="form-control name" id="name_'+x+'" placeholder="Name" name="name[]" data-id="'+x+'"><span class="error"></span></td>' +
                '<td><input type="number" class="form-control quantity itemvalue" id="quantity_'+x+'" placeholder="Quantity" name="quantity[]" data-id="'+x+'" min="0" oninput="this.value = Math.abs(this.value)"><span class="error"></span></td>' +
                '<td><div class="input-group mb-2" id="input-group_'+x+'"><div class="input-group-prepend"><div class="input-group-text">$</div></div><input type="number" class="form-control price itemvalue" id="price_'+x+'" placeholder="Price" name="price[]" data-id="'+x+'" min="0" oninput="this.value = Math.abs(this.value)"></div><span class="error"></span></td>' +
                '<td><select class="form-control tax itemvalue" id="tax_'+x+'" name="tax[]" data-id="'+x+'"><option value="0" selected>0%</option><option value="1">1%</option><option value="5">5%</option><option value="10">10%</option></select></td>' +
                '<td id="total_'+x+'"></td>' +
                '<td class="icon-groups"><input type="hidden" class="sub-without-tax" id="sub-without-tax_'+x+'" value=""><input type="hidden" class="sub-with-tax" id="sub-with-tax_'+x+'" value=""><a href="javascript:void(0);" class="btn btn-primary removerow" data-id="'+x+'" id="removerow'+x+'"><i class="fa fa-minus"></i></a></td>' +
                '</tr>');
            x++;
        }

    });

    $(wrapper).on("click",".removerow", function(e){
        e.preventDefault();
        $(this).parent('td').parent('tr').remove();
        var withouttax = 0;
        $("input[class *= 'sub-without-tax']").each(function(){
            withouttax += +$(this).val();
        });

        var withtax = 0;
        $("input[class *= 'sub-with-tax']").each(function(){
            withtax += +$(this).val();
        });

        $("#total-without-tax").html('$'+withouttax.toFixed(2));
        $("#total-with-tax").html('$'+withtax.toFixed(2));
        $("#sub-with-tax-total").val(withtax.toFixed(2));

        var discountvalue = +($("#discount-value").val());

        if(discountvalue==''){
            $("#total-amt-text").html('$'+withtax.toFixed(2));
            $("#total-amt").val(withtax.toFixed(2));
        }
        else{
            discountcalculation();
        }
        x--;
    });
});

$(document).on( "change", ".itemvalue", function(){

    var id = $(this).attr("data-id");

    var quantity = $('#quantity_'+id).val();

    var price = $('#price_'+id).val();

    var tax = 0;

    var amount = 0;

    if(quantity!='' && price!=''){

        amount = (quantity * price);

        var taxvalue = $('#tax_'+id).val();

        tax = amount * (taxvalue/100);

        var totalamt = (amount + tax).toFixed(2);

        $('#total_'+id).html(totalamt);

        $('#sub-without-tax_'+id).val(amount.toFixed(2));

        $('#sub-with-tax_'+id).val(totalamt);

        var withouttax = 0;
        $("input[class *= 'sub-without-tax']").each(function(){
            withouttax += +$(this).val();
        });

        var withtax = 0;
        $("input[class *= 'sub-with-tax']").each(function(){
            withtax += +$(this).val();
        });

        $("#total-without-tax").html('$'+withouttax.toFixed(2));
        $("#total-with-tax").html('$'+withtax.toFixed(2));
        $("#sub-with-tax-total").val(withtax.toFixed(2));

        var discountvalue = +($("#discount-value").val());

        if(discountvalue==''){
            $("#total-amt-text").html('$'+withtax.toFixed(2));
            $("#total-amt").val(withtax.toFixed(2));
        }
        else{
            discountcalculation();
        }

    }

});

$(document).on( "change", ".discount-value", function(){

    discountcalculation();

});   

$(document).on( "change", "#w0 input[type='radio'][name=discount-radio]", function(){
    discountcalculation();
});

function discountcalculation() {

    var discount = '';

    var total = '';

    total = +($("#sub-with-tax-total").val());

    discount = +($("#discount-value").val());

    var radiovalue = $("#w0 input[type='radio']:checked").val();

    if(radiovalue==0 && total>discount){
        var totalamt = total - discount;
        $("#total-amt-text").html('$'+totalamt.toFixed(2));
        $("#total-amt").val(totalamt.toFixed(2));
    }
    else if(radiovalue==0 && total<=discount){
        $("#discount-value").val('');
        $("#total-amt-text").html('$'+total.toFixed(2));
        $("#total-amt").val(total.toFixed(2));
        alert('Discount greater than or equal to total')
    }

    if(radiovalue==1 && total>discount){
        var totalamt = total - (total*(discount/100));
        $("#total-amt-text").html('$'+totalamt.toFixed(2));
        $("#total-amt").val(totalamt.toFixed(2));
    }
    else if(radiovalue==1 && total<=discount){
        $("#discount-value").val('');
        $("#total-amt-text").html('$'+total.toFixed(2));
        $("#total-amt").val(total.toFixed(2));
        alert('Discount greater than or equal to total')
    }

}


$(document).on("click", "#save-invoice", function (e) {

    e.preventDefault();

    var error=0;

    $('.error').hide();

    var invoiceno = $("#invoiceno").val();

    var invoicedate = $("#datepicker").val();

    if(invoiceno==''){
        $("#invoiceno").next("span").html('Enter Invoice No').show('slow').delay(5000).fadeOut();
            error=1;
    }

    if(invoicedate==''){
        $("#datepicker").next("span").html('Select Date').show('slow').delay(5000).fadeOut();
            error=1;
    }

    $('.name').each(function(){
        var idCheck = $(this).attr("data-id");
        if($(this).val()=='')
        {
            $("#name_"+idCheck).next("span").html('Enter Name').show('slow').delay(5000).fadeOut();
            error=1;
        }
    });

    $('.quantity').each(function(){
        var idCheck = $(this).attr("data-id");
        if($(this).val()=='')
        {
            $("#quantity_"+idCheck).next("span").html('Enter Quantity').show('slow').delay(5000).fadeOut();
            error=1;
        }
    });

    $('.price').each(function(){
        var idCheck = $(this).attr("data-id");
        if($(this).val()=='')
        {
            $("#input-group_"+idCheck).next("span").html('Enter Price').show('slow').delay(5000).fadeOut();
            error=1;
        }
    });

    if (error == 0)
    { 

        $.ajax({
            type: 'POST',
            url: '../invoice/create',
            dataType:"json",
            async: false,
            data:$('#w0').serialize(),
            success: function(data){

                

            }
        });

    }
    
});