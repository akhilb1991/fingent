<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "items".
 *
 * @property int $item_id
 * @property int $invoice_id primary key of invoice table
 * @property string $name
 * @property float $quantity
 * @property float $price
 * @property int $tax
 * @property int $created_by
 * @property string $created_at
 * @property int $status
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'name', 'quantity', 'price', 'tax'], 'required'],
            [['invoice_id', 'tax', 'created_by', 'status'], 'integer'],
            [['quantity', 'price'], 'number'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'item_id' => 'Item ID',
            'invoice_id' => 'Invoice ID',
            'name' => 'Name',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'tax' => 'Tax',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }
}
