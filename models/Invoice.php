<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "invoice".
 *
 * @property int $invoice_id
 * @property string $invoice_no
 * @property string $invoice_date
 * @property string $discount
 * @property float $amount
 * @property int $status 0:Active,1:Deleted
 * @property int $created_by user id
 * @property int $created_at
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_no', 'invoice_date'], 'required'],
            [['invoice_date'], 'safe'],
            [['amount'], 'number'],
            [['status', 'created_by', 'created_at'], 'integer'],
            [['invoice_no', 'discount'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'invoice_no' => 'Invoice No',
            'invoice_date' => 'Invoice Date',
            'discount' => 'Discount',
            'amount' => 'Amount',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }
}
